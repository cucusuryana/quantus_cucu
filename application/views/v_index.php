<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="<?= base_url() ?>assets/purple/assets/images/favicon.ico">

        <title>Dashboard</title>
       
        <!-- PLUGIN-->
        <!-- Multi Item Selection examples -->
        <link href="<?= base_url() ?>assets/purple_hori/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/purple_hori/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/purple_hori/assets/css/style.css" rel="stylesheet" type="text/css" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        
    </head>
<div class="wrapper">
            <div class="container-fluid">
          <?php $this->load->view($main_view); ?>   
            </div> <!-- end container -->
        </div>
     
        <!-- jQuery  -->
        <script src="<?= base_url() ?>assets/purple_hori/assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/purple_hori/assets/js/bootstrap.min.js"></script>

       <script src="<?= base_url() ?>assets/purple_hori/assets/js/jquery.core.js"></script>
      
    </body>
</html>