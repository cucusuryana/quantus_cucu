     <div class="row">
                    <div class="col-12">
                        <div class="card-box">
						<form class="form-horizontal" role="form">
                                            <div class="form-group row">
                                                <label class="col-2 col-form-label">Nama Barang</label>
                                                <div class="col-10">
                                                    <input type="text" class="form-control" id="name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-2 col-form-label" for="example-email">Harga</label>
                                                <div class="col-10">
                                                    <input type="text" class="form-control" id="price">
                                                </div>
                                            </div>
											 <div class="form-group row">
                                                <label class="col-2 col-form-label" for="example-email">Jumlah</label>
                                                <div class="col-10">
                                                     <input type="text" class="form-control" id="qty">
                                                </div>
                                            </div>
											 <div class="form-group row">
                                                <label class="col-2 col-form-label" for="example-email"></label>
                                                <div class="col-10">
                                                     <input type="button" class="btn btn-default btn-rounded waves-effect waves-light add-row" value="Tambah">
                                                </div>
                                            </div>
                                        </form>
					

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="myTable">
                                    <thead>
                                    <tr>
                                        
                                        <th class="text-center">
                                           NO
                                        </th>
                                        <th class="text-center">
                                          Nama  Barang
                                        </th>
                                        <th class="text-center">
                                           Harga
                                        </th>
                                        <th class="text-center">
                                           Jumlah
                                        </th>
                                        <th class="text-center">
                                           Total
                                        </th>
										<th class="text-center">
                                           Hapus
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
								  <?php 
									$id=1;
									foreach($myData as $key => $value): ?>
										<tr>
										<?php
										$total =  $value['jumlah'] * $value['harga'];
										?>
											<td><?php echo $id; ?></td>
											 <td><?php echo $value['nama']; ?></td>
											<td ><?php echo $value['harga']; ?></td>
											<td><?php echo $value['jumlah']; ?></td>
											<td class="amount"><?php echo $total; ?></td>
											<td><button type="button" class="btn btn-danger btn-rounded waves-effect waves-light delete">X</button></td>
										</tr>
									<?php 
									$id++;
									endforeach; ?>
                                       
                                       
                                    </tr>
                                   
                                    </tbody>
									<tfoot>
										<tr>
											<td colspan="4">Total:</td>
											<td class="total"></td>
											<td></td>
										</tr>
										<tr>
											<td colspan="4">Paling Banyak:</td>
											<td class="paling_banyak"></td>
											<td></td>
										</tr>
										<tr>
											<td colspan="4">Paling mahal:</td>
											<td class="paling_mahal"></td>
											<td></td>
										</tr>
									</tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->
				
<script type="text/javascript">

   function calculateColumn(index) {
            var total = 0;
            $('table tr').each(function() {
                var value = parseInt($('.amount', this).eq(index).text());
                if (!isNaN(value)) {
                    total += value;
                }
            });
            $('.total').eq(index).text(total);
        }
	$(document).ready(function() {
				$('table thead th').each(function(i) {
					calculateColumn(i);
				});
	});
	$(document).on("click", ".delete", function(){
		$(this).parents("tr").remove();
		  $('table thead th').each(function(i) {
				calculateColumn(i);
			});
		
	});
	
    $(document).ready(function(){
	    var totrow = $('#myTable tr').length  -4;
        $(".add-row").click(function(){
		   
			var rowCount = totrow +1;
            var name = $("#name").val(),
				price = $("#price").val(),
				qty = $("#qty").val(),
				amount = qty * price,
				markup = "<tr><td>"+ rowCount +"</td><td>" + name + "</td><td>" + price + "</td><td>" + qty + "</td><td class='amount'>" + amount + "</td><td> <button type='button' class='btn btn-danger btn-rounded waves-effect waves-light delete'>X </button></td></tr>";
            $("table tbody").append(markup);
			
            $('table thead th').each(function(i) {
                calculateColumn(i);
            });
        });
     
    });    
	/*  var cols = []
	  var trs = $('#myTable tr')
	  var data =$.each(trs , function(index, tr){
		$.each($(tr).find("td").not(":first"), function(index, td){
		  cols[index] = cols[index] || [];
		  cols[index].push($(td).text())
		})
	  });
	  cols.forEach(function(col, index){
		var max = Math.max.apply(null, col);
		var min = Math.min.apply(null, col)
		$('#myTable tr').find('td:eq('+(index+1)+')').each(function(i, td){
		 // $(td).toggleClass('max', +$(td).text() === max)
		  //$(td).toggleClass('min', +$(td).text() === min)  
		})
	  }) */
</script>