<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        
        
        // cek status login user
        if ($this->session->userdata('login') == FALSE)
        {
            redirect('login');
        }
		  $query = $this->db->where('id_user', $this->session->userdata('id_user'))
                          ->get('user');  
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();  
            
            $username = $data["username"];
			$this->session->set_userdata('username', $username);
		}
    }   
}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */